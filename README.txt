The twitter_blackbird module is a port of Brad Vincent's
"Blackbird Pie" WordPress plugin. The module allows you to 
create an HTML representation of actual tweets in your 
posts, just by pasting in a tweet URL. 

Twitter API:

  http://media.twitter.com/blackbird-pie/

Demo:

  http://www.universalpantograph.com/content/test-blackbird-pie-tweet-embedding-drupal-module

In addition, each HTML representation includes web intent links:

  http://dev.twitter.com/pages/intents

that allow you to reply, retweet, or favorite the tweet. 

NOTE Because Drupal stores only data that is entered in 
posts, only the URL (and not the full tweet HTML 
representation) is stored in content. This may change in a 
future release. However, the twitter ID and twitter 
details are stored, so that the site only hits twitter once 
per posted tweeet.

NOTE Use the core URL filter to make URLs clickable.

Author
------
Sam Hunting
shunting@universalpantograph.com
http://www.universalpantograph.com